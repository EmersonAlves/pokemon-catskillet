import { api } from "./api";

export default {
  list(offset) {
    return api.get(`pokemon?limit=20&offset=${offset}`);
  },
  getImageById(id) {
    return `https://pokeres.bastionbot.org/images/pokemon/${id}.png`;
  },
  getPokemonDataByName(name) {
    return api.get(`https://pokeapi.co/api/v2/pokemon/${name}`);
  },
};
